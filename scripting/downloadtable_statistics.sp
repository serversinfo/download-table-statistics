#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#pragma newdecls required

#define VERSION "1.2"



/*****************************************************************


			P L U G I N   I N F O


*****************************************************************/

public Plugin myinfo = {
	name = "Download Table Statistics",
	author = "Berni",
	description = "Shows statistics and missing files of the hl2 download table",
	version = VERSION,
	url = "http://forums.alliedmods.net/showthread.php?t=75555"
}



/*****************************************************************


			G L O B A L   V A R S


*****************************************************************/

Handle dts_version;

public void OnPluginStart() {
	// ConVars
	dts_version = CreateConVar("dts_version", VERSION, "Download Table Statistics plugin version", FCVAR_DONTRECORD|FCVAR_REPLICATED|FCVAR_NOTIFY);
	// Set it to the correct version, in case the plugin gets updated...
	SetConVarString(dts_version, VERSION);

	RegAdminCmd("sm_downloadtablestatistics", DownloadTableStatistics, ADMFLAG_CUSTOM4);
	RegAdminCmd("sm_dts", DownloadTableStatistics, ADMFLAG_CUSTOM4);
}

/****************************************************************
			C A L L B A C K   F U N C T I O N S
****************************************************************/

public Action DownloadTableStatistics(int client, int args) {
	char arg1[32];
	char path[PLATFORM_MAX_PATH];
	char path_bz2[PLATFORM_MAX_PATH];
	char part[32];
	float filesize;

	float size_sounds;
	float size_maps;
	float size_models;
	float size_materials;
	float size_others;
	
	int count_notfound;
	int count_sounds;
	int count_maps;
	int count_models;
	int count_materials;
	int count_others;
	int count_bzip2;

	static int table = INVALID_STRING_TABLE;
	
	if (table == INVALID_STRING_TABLE)
		table = FindStringTable("downloadables");
	
	GetCmdArg(1, arg1, sizeof(arg1));
	
	ReplyToCommand(client, "[SM] Displaying statistics for the download table:");
	
	int size = GetStringTableNumStrings(table);
	bool bShowPath = StrEqual(arg1, "showpaths");
	if (!bShowPath)
		ReplyToCommand(client, "[SM] Use \"sm_dts showpaths\" for full list");
		
	for (int i=0; i<size; ++i) {
		bool isBzip2File = true;

		ReadStringTable(table, i, path, sizeof(path));
		
		Format(path_bz2, sizeof(path_bz2), "%s.bz2", path);
		
		filesize = float(FileSize(path_bz2));
		
		if (filesize == -1) {
			filesize = float(FileSize(path));
			isBzip2File = false;
		}
		if(bShowPath && filesize != -1)
			ReplyToCommand(client, "[SM] %-6.3f Mb Path: %s ", filesize/1024/1024, path);
		
		if (filesize == -1) {
			ReplyToCommand(client, "[SM] Warning: File not found : %s", path);
			count_notfound++;
		} else {
			if (isBzip2File)
				count_bzip2++;

			filesize = filesize/1024/1024;
			
			int n = 0;
			while (path[n] != '/' && path[n] != '\\' && path[n] != '\0') {
				part[n] = path[n];
				n++;
			}
			part[n] = '\0';
			
			bool other = true;
			
			if (path[0] != '\0') {
				if (StrEqual(part, "sound", true)) {
					size_sounds += filesize;
					count_sounds++;
					other = false;
				}
				else if (StrEqual(part, "maps", true)) {
					size_maps += filesize;
					count_maps++;
					other = false;
				}
				else if (StrEqual(part, "models", true)) {
					size_models += filesize;
					count_models++;
					other = false;
				}
				else if (StrEqual(part, "materials", true)) {
					size_materials += filesize;
					count_materials++;
					other = false;
				}
			}
			if (other) {
				size_others = filesize;
				count_others++;
			}
		}
	}
	bShowPath = false;
	
	float totalsize = size_sounds+size_maps+size_models+size_materials+size_others;
	
	ReplyToCommand(client, "[SM] Sounds    (%4d):  %-6.3f mb", count_sounds, size_sounds);
	ReplyToCommand(client, "[SM] Maps      (%4d):  %-6.3f mb", count_maps, size_maps);
	ReplyToCommand(client, "[SM] Models    (%4d):  %-6.3f mb", count_models, size_models);
	ReplyToCommand(client, "[SM] Materials (%4d):  %-6.3f mb", count_materials, size_materials);
	ReplyToCommand(client, "[SM] Others    (%4d):  %-6.3f mb", count_others, size_others);
	ReplyToCommand(client, "[SM] ------");
	ReplyToCommand(client, "[SM] Total files: %d (%d not found): %.3f mb", size,  count_notfound, totalsize);
	ReplyToCommand(client, "[SM] Compressed Files (bzip2): %d", count_bzip2);
	ReplyToCommand(client, "[SM] min. estimated download time: 56k: %.2fmin   2mbit: %.2fmin   4mbit: %.2fmin   16mbit: %.2fmin", calcDownloadTime(totalsize, 0.0546875), calcDownloadTime(totalsize, 2.0), calcDownloadTime(totalsize, 4.0), calcDownloadTime(totalsize, 16.0));
	
	bool save = LockStringTables(false);

	LockStringTables(save);

}

/*****************************************************************
			P L U G I N   F U N C T I O N S
*****************************************************************/

stock float calcDownloadTime(float filesize, float speed) {
	float filesize_mbits = filesize*8;
	float secs = filesize_mbits/speed;
	float mins = secs/60;
	
	return mins;
}

stock int strfind(char[] str, int charr) {
	int n=0;
	
	while (str[n] != '\0')
		if (str[n] == charr)
			return n;
	
	return -1;
}
